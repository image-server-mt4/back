# back

Repository that host our backend for the realization of a CI pipeline of tests with Gitlab.

## Install

First clone the repository, then run

### Without Docker
```bash
pip3 install -r src/requirements.txt
python3 src/app.py
```

### With Docker

```bash
docker build -t <container-name> .

docker run <container-name>
```

## Author

Edwin Vautier
